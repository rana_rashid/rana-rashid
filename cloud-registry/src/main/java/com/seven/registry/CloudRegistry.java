package com.seven.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author rrs
 */

@SpringBootApplication
@EnableEurekaServer
public class CloudRegistry {
  public static void main (String[] args) {
    SpringApplication.run(CloudRegistry.class, args);
  }
}
