package com.seven.email.service;

import com.seven.email.request.EmailMessage;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

/**
 * @author rrs
 */
@Service
@RefreshScope
@Log4j2
public class EmailSender {

  @Autowired
  private JavaMailSender mailSender;

  @Value("${spring.mail.username}")
  private String emailFrom;

  @RabbitListener(queues = "HP-QUEUE")
  public void receiveHPMessage(final EmailMessage customMessage) {
    sendMessage(customMessage);
  }

  @RabbitListener(queues = "HP-QUEUE")
  public void receiveLPMessage(final EmailMessage customMessage) {
    sendMessage(customMessage);
  }

  private void sendMessage(EmailMessage customMessage){
    try {
      log.debug("Email has been received in Queue {}", customMessage.getTo());
      final String subject = customMessage.getSubject();
      MimeMessage message = mailSender.createMimeMessage();
      MimeMessageHelper helper = new MimeMessageHelper(message, true);
      helper.setTo(customMessage.getTo().split(","));
      helper.setFrom(emailFrom);
      helper.setSubject(subject);
      helper.setText(customMessage.getBody(), true);
      mailSender.send(message);
      log.debug("Sent email");
    } catch (Exception e) {
      log.catching(e);
    }
  }

}
