package com.seven.email;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

/**
 * @author rrs
 */

@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties
@Configuration
@EnableRabbit
public class EmailApplication {

  public static void main (String[] args) {
    SpringApplication.run(EmailApplication.class, args);
  }

}
