package com.seven.email.controller;

import com.seven.email.request.EmailMessage;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;

/**
 * @author rrs
 */
@RestController
@Log4j2
public class EmailController {

  @Autowired
  private RabbitTemplate rabbitTemplate;

  @ResponseBody
  @RequestMapping (value = "/sendEmail", method = RequestMethod.POST)
  public String sendEmail (@Valid @RequestBody EmailMessage emailMessage) {
    if(Objects.isNull(emailMessage.getTo())){
          return "To can not be empty";
    }
    if(Objects.isNull(emailMessage.getSubject())){
        return "Subject can not be empty";
    }
    if(Objects.isNull(emailMessage.getBody())){
        return "Body can not be empty";
    }
    if(!Objects.isNull(emailMessage.getPriority()) && emailMessage.getPriority().equalsIgnoreCase("HIGH")){
        rabbitTemplate.setQueue("HP-QUEUE");
    }else{
        rabbitTemplate.setQueue("LP-QUEUE");
    }
    if(emailMessage.getTimeToSendEmail() != 0){
        rabbitTemplate.setQueue("LP-QUEUE");
        rabbitTemplate.setReceiveTimeout(emailMessage.getTimeToSendEmail());
    }
    rabbitTemplate.convertAndSend("TOPIC-EXCHANGE", "ROUTING-KEY", emailMessage);
    return "Email has been sent successfully";
  }

}
