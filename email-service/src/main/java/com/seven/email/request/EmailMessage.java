package com.seven.email.request;

import lombok.Data;

@Data
public class EmailMessage {

    private String subject;
    private String to;
    private String body;
    private String priority="HIGH";
    private long timeToSendEmail=0;

}
