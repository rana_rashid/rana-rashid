package com.seven.email.exception;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author rrs
 */
@ControllerAdvice
@Log4j2
public class GlobalHandler {

  @ExceptionHandler(Exception.class)
  @ResponseBody
  public String exceptionHandler(Exception exception) {
    log.catching(exception);
    return exception.getMessage();
  }

}
