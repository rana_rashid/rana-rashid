## Infrastructural services
This is orchestrated with Spring Cloud (a.k.a microservices), each component providing
features related to the module. Modules communicate with each other by consuming rest services provided by the module.

Following are the core modules:

#### 1. Configuration (Cloud-Config)

This module is mainly to deal with the centralized configuration of all modules. We have used `spring-cloud-config-server` provided
by the Spring Cloud which is used to handle the configuration and hand it over to the related module. Centralizing 
configuration can be done via File System, Git or Native.

```yaml
spring:
  cloud:
    config:
      server:
        native:
          search-locations: classpath:/shared
          
  profiles:
       active: native
```

This is how configuration is managed as native profile and the location we have told it to be is: `classpath:/shared`.
This will tell Spring Cloud Config to load properties (configuration) for the module from the location. I now prefer `.yml`
over `.properties` file because of the ease of readability. :thumbsup_tone1:


Couple of things about Cloud Configuration and it's settings. If you see `application.yml` file in 
`src/main/resources`, this configuration is solely for cloud configuration purpose. Then there are
configuration files inside `shared` folder which are shared with relevant module which requests for it.

#### 2. Registry (Cloud-Registry)

Spring cloud offers registry service (discovery service) which can be used to bind multiple services all together. So, the basic
purpose of discovery (registry) service is to let all services register with some centralized service so that each service is known
to the central service and depending on that, you communicate with the services via URL or Service Name. You can scale services
horizontally as well as vertically. :computer:

So, as said above, it requires a central service with which all other services will register and that will be the central
point of communication for all the services. There are two main implementations Eureka by Netflix and Consul by HashiCorp.
Spring provided API's wrapped on both of them and we will be using Eureka instead of Consul.

```pom
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-eureka-server</artifactId>
        </dependency>
```

And since we need our registry module to fetch all configurations from Cloud-Config which we have configured above, we
will add cloud config dependency as well:

```pom
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-config</artifactId>
        </dependency>
```

We do need to inform cloud-config that which file you need to serve when you Cloud-Registry module asks for configurations.
Just add `bootstrap.yml` file in Cloud-Registry. 

##### Why `bootstrap.yml` and not `application.yml`? :closed_lock_with_key:

Because `application.yml` for every module is already configured in `shared/application.yml` and 
`bootstrap.yml` is used to inform what other file for configuration you need to load for me.

```yaml
spring:
  application:
    name: cloud-registry
```

This piece of code tells cloud to load `cloud-registry.yml` file from `shared/` folder which is located 
on classpath as already done in cloud configuration.

Then we have also told this configuration cloud config user and password.

```yaml
cloud:
    config:
      uri: http://cloud-config:8888
      fail-fast: true
      password: ${CONFIG_SERVICE_PASSWORD}
      username: user
```

And since this is the registry server itself so we don't need it to register with it's own self.

```yaml
eureka:
  instance:
    prefer-ip-address: true
  client:
    registerWithEureka: false
    fetchRegistry: false
    server:
      waitTimeInMsWhenSyncEmpty: 0
```

#### 3. Gateway (Cloud-Gateway)

The only module that will be exposed to the outside world will be the `cloud-gateway` module. Spring provides
Zuul API to configure your own gateway that acts as proxy for other services. All communication will be done
from outside world with this module and this module will be responsible for sending requests to relevant modules
configured behind the proxy. :thumbsup_tone1:

`bootstrap.yml` is configured as usual to register it's name and fetch the properties as `cloud-gateway.yml`.

Main configuration is placed in `cloud-gateway.yml` as:

```yaml
zuul:
  ignoredServices: '*'
  host:
    connect-timeout-millis: 20000
    socket-timeout-millis: 20000
```

This informs that all of the services are ignored and `zuul` will not intercept any of the services, **except**

```yaml
    email-service:
        path: /email/**
        serviceId: email-service
        stripPrefix: false
        retryable: true
        sensitiveHeaders:
```

If the URL request is `/email/`, requests will be handed over to email service for processing and response
will be sent back to the client.

`sensitiveHeaders` is required since we don't require `zuul` to ignore any of the headers being send in the request 
or return back to response.

If you observe, we have followed two different patterns of handing the control over to related services. :prince:

1. `url: http://auth-service:5000`
    
    This is the url type of handing over to the related service.
    
2. `serviceId: email-service`

    This is the service style of handing over to the related service.
    

## Functional services

Following are the core functional services provided

#### 1. Email (email-service)

This service mainly used [RabbitMQ](https://www.rabbitmq.com/) an [AMQP](http://www.amqp.org/) protocol 
implementation. This service requires RabbitMQ to be installed and running (or run a docker instance).

Following configuration

```yaml
spring:
  rabbitmq:
    host: rabbitmq
  mail:
      host: smtp.gmail.com
      port: 587
      username: ${FROM_EMAIL_ADDRESS}
      password: ${FROM_EMAIL_PASSWORD}
      properties:
        mail:
          smtp:
            auth: true
            starttls:
              enable: true
              required: true
```

is used to configure RabbitMQ and also the email settings from which actually the email will be sent.
This service only exposes one endpoint (can be extended if required) `/sendEmail` 

**`/sendEmail`** is a `POST` method endpoint which requires `JSON` in following format:

```json
{
  "to": "a comma separated, list of email addresses",
  "subject": "SUBJECT",
  "body": "I have not tested the limit of body yet but backend needs to make sure to restrict it to a certain limit."
}
```


### Infrastructure Automation

To automate the infrastructure, I have integrated `Dockerfile` in each module so it's just one command
and the whole infrastructure will be available to you in few seconds.

##### Endpoints

Once docker is started, you need to understand that following are the endpoints which are open:

1. Gateway [http://localhost](http://localhost)

2. Service Registry [http://localhost:8761](http://localhost:8761)

3. RabbitMQ [http://localhost:15672](http://localhost:15672)